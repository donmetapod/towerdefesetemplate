﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Indestructible : MonoBehaviour
{

	private static Indestructible indestructible;
	public string tmp = "hola";

	public delegate void OnMessageReceived();
	public event OnMessageReceived onMessageReceived;
	
	private void Awake()
	{
		DontDestroyOnLoad(this);
		if (indestructible == null)
		{
			indestructible = this;
		}
		else
		{
			Destroy(gameObject);
		}
	}

	private void Start()
	{
		//StartCoroutine(MultipleWait());
		StartCoroutine(FirstRoutine());
		
	}

	private void OnEnable()
	{
		onMessageReceived += PrintMessage;
		onMessageReceived += PrintPosition;
	}

	private void OnDisable()
	{
		onMessageReceived -= PrintMessage;
		onMessageReceived -= PrintPosition;
	}

	void PrintMessage()
	{
		Debug.Log(transform.name);
	}

	void PrintPosition()
	{
		Debug.Log(transform.position);
	}

	IEnumerator MultipleWait()
	{
		while (true)
		{
			yield return new WaitForSeconds(1);
			Debug.Log(Time.time);
			yield return new WaitForSeconds(3);
			Debug.Log(Time.time);
			yield return new WaitForSeconds(4);
			Debug.Log(Time.time);	
		}
	}

	IEnumerator FirstRoutine()
	{
		Debug.Log("First");
		yield return StartCoroutine(SecondRoutine());
		if (onMessageReceived != null)
		{
			onMessageReceived();
		}

		
	}

	IEnumerator SecondRoutine()
	{
		yield return new WaitForSeconds(3);
	}


	void Update()
	{
		if (Input.GetMouseButtonDown(0))
		{
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;

			if (Physics.Raycast(ray, out hit))
			{
				TPDamageable damage = hit.transform.GetComponent<TPDamageable>(); 
				if (damage != null)
				{
					damage.TakeDamage(1);
				}
			}
		}
	}

}
