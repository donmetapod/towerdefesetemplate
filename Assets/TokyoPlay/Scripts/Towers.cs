﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace tec
{
	public class Towers : MonoBehaviour
	{

		[SerializeField] private Transform target;
		[SerializeField] private int searchTime;
		[SerializeField] private int range = 10;
		[SerializeField] private int power = 1;
		[SerializeField] private Transform turret;
	
		private void Start()
		{
			StartCoroutine(CheckForTarget());
			StartCoroutine(ShootTarget());
		}

		IEnumerator CheckForTarget()
		{
			while (true)
			{
				yield return new WaitForSeconds(searchTime);
				if (target == null)
				{
					if (GameManager.Instance.enemyList.Count > 0)
					{
						float closestDistance = Mathf.Infinity;
						foreach (var enemy in GameManager.Instance.enemyList)
						{
							float distance = Vector3.Distance(transform.position, enemy.position);
							if (distance < range)
							{
								if (distance < closestDistance)
								{
									closestDistance = distance;
									target = enemy;
								}
							}
						}
					}
				}
			}
		}

		IEnumerator ShootTarget()
		{
			while (true)
			{
				yield return new WaitForSeconds(1);
				if (target != null)
				{
					target.GetComponent<Enemy>().TakeDamage(power);
				}
			}
		}

		private void Update()
		{
			if (target != null)
			{
				turret.transform.LookAt(target);
			}
		}
	}
	

}
