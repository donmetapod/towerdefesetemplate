﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneButton : MonoBehaviour {

	public void Load(string sceneName)
	{
		SceneManager.LoadScene(sceneName);
	}

	private void OnEnable()
	{
		FindObjectOfType<Indestructible>().onMessageReceived += AnotherMethod;
	}

	private void OnDisable()
	{
		FindObjectOfType<Indestructible>().onMessageReceived -= AnotherMethod;
	}

	public void AnotherMethod()
	{
		Debug.Log("Another method");
	}
}
