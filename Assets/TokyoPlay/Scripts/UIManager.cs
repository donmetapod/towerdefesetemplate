﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Internal.Experimental.UIElements;
using UnityEngine.UI;

namespace tec
{
	public class UIManager : MonoBehaviour
	{

		private static UIManager tpuiManager; 

		public static UIManager Instance
		{
			get
			{
				return tpuiManager;
			
			}
			set
			{
				tpuiManager = value; 
			}
		}

		[SerializeField] private GameObject startWaveButton;
		[SerializeField] private GameObject wavesStats;
		[SerializeField] private Text waveNumber;
		[SerializeField] private Slider waveProgressSlider;
		[SerializeField] private Text playerEnergy;
		public GameObject gameOverPanel;
		public Text gameOverText;
		public Text hqHealth;
	
		private void Awake()
		{
			if (Instance == null)
			{
				Instance = this;
			}
			else
			{
				Destroy(gameObject);
			}
		}

		public void InitializeWaves()
		{
			startWaveButton.SetActive(false);
			wavesStats.SetActive(true);
			StartCoroutine(FindObjectOfType<EnemySpawner>().SendWave());
			StartCoroutine(GameManager.Instance.IncreaseEnergy());
		}

		public void UpdateWaveNumber(int currentWave, int totalWaves)
		{
			waveNumber.text = "Wave " + currentWave + "/" + totalWaves;
			if (waveProgressSlider.value > 98)
			{
				waveProgressSlider.value = 0;
			}
		}

		public IEnumerator UpdateWaveProgress(int currentEnemy, int totalEnemies)
		{
			float progress = (100 / totalEnemies) * currentEnemy;
			while (waveProgressSlider.value < progress)
			{
				waveProgressSlider.value += Time.deltaTime * 20;
				yield return new WaitForEndOfFrame();
			}
		}

		public void CeateWeapon(string weaponType)
		{
			FindObjectOfType<PLayer>().CreateWeapon(weaponType);
		}

		public void UpdateEnergyUI()
		{
			playerEnergy.text = GameManager.Instance.playerEnergy.ToString();
		}

		public void TogglePause()
		{
			if (Time.timeScale > 0)
			{
				Time.timeScale = 0;
			}
			else
			{
				Time.timeScale = 1;
			}
		}
	}
	

}
