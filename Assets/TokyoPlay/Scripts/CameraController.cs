﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace tec
{
    public class CameraController : MonoBehaviour {

        [SerializeField] private Vector2 mousePos;
        [SerializeField] private float screenWidth;
        [SerializeField] private float screenHeight;
        [SerializeField] private float cameraMoveSpeed = 5;
	
        void Update () {
            MoveCamera();
        }

        void MoveCamera()
        {
            mousePos = Input.mousePosition;
            screenWidth = Screen.width;
            screenHeight = Screen.height;

            Vector3 cameraPos = transform.position;
		
            if (mousePos.x < (screenWidth * .10f) && cameraPos.z < 10)
            {
                cameraPos.z += Time.deltaTime * cameraMoveSpeed;
            }

            if (mousePos.x > (screenWidth * .90f) && cameraPos.z > 0)
            {
                cameraPos.z -= Time.deltaTime * cameraMoveSpeed;
            }

            if (mousePos.y < (screenHeight * .10f) && cameraPos.x > -30)
            {
                cameraPos.x -= Time.deltaTime * cameraMoveSpeed;
            }

            if (mousePos.y > (screenHeight * .90f) && cameraPos.x < -19)
            {
                cameraPos.x += Time.deltaTime * cameraMoveSpeed;
            }
		
            transform.position = cameraPos;
        }
    }

}

