﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

	[SerializeField] private GameObject gunPrefab;
	[SerializeField] private GameObject missilePrefab;
	[SerializeField] private GameObject sniperPrefab;

	[SerializeField] private GameObject holdingWeapon;
	
	public void CreateWeapon(string weapon)
	{
		switch (weapon)
		{
				case "gun":
					holdingWeapon = Instantiate(gunPrefab);
					break;
		}
	}

	private void Update()
	{
		UpdateHoldingWeapon();
	}

	void UpdateHoldingWeapon()
	{
		if (holdingWeapon != null)
		{
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			
			if (Physics.Raycast(ray, out hit))
			{
				Debug.DrawRay(ray.origin, hit.point);
				
				holdingWeapon.transform.position = hit.point;


				if (Input.GetMouseButtonDown(0) && hit.transform.CompareTag("Placement"))
				{
					holdingWeapon = null;
				}
			}
		}
	}
}
