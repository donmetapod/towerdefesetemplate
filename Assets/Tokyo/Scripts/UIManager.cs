﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
	private static UIManager uiManager;

	public static UIManager Instance
	{
		get { return uiManager; }
		set { uiManager = value; }
	}

	[SerializeField] private GameObject startWaveButton;
	[SerializeField] private GameObject waveStats;
	[SerializeField] private Text waveNumber;
	[SerializeField] private Slider waveProgressSlider;

	private void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
		}
		else
		{
			Destroy(gameObject);
		}
	}

	public void StartWaves()
	{
		startWaveButton.SetActive(false);
		waveStats.SetActive(true);
		StartCoroutine(FindObjectOfType<EnemySpawner>().SendWave());
	}

	public void UpdateWaveNumber(int currentWave, int totalWaves)
	{
		waveNumber.text = "Wave " + currentWave + "/" + totalWaves;
		if (waveProgressSlider.value > 98)
		{
			waveProgressSlider.value = 0;
		}
	}

	public IEnumerator UpdateWaveProgress(int currentEnemy, int totalEnemies)
	{
		float progress = (100 / totalEnemies) * currentEnemy;
		while (waveProgressSlider.value < progress)
		{
			waveProgressSlider.value += Time.deltaTime * 10;
			yield return new WaitForEndOfFrame();
		}
	}
}
