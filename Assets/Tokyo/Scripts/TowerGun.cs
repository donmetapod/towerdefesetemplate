﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerGun : MonoBehaviour
{

	[SerializeField] private Transform turret;
	[SerializeField] private Transform target;
	
	void Start () {
		
	}
	
	
	void Update () {
		if (target != null)
		{
			turret.LookAt(target);
		}
	}
}
