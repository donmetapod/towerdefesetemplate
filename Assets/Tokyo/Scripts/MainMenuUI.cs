﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuUI : MonoBehaviour
{

    [SerializeField] private GameObject mainMenuPanel;
    [SerializeField] private GameObject levelSelectPanel;
    [SerializeField] private Animator anim;
    
    public void ShowLevelSelect()
    {
        mainMenuPanel.SetActive(false);
        levelSelectPanel.SetActive(true);
        anim.SetTrigger("ToLevelSelect");
    }

    public void ShowMainMenu()
    {
        mainMenuPanel.SetActive(true);
        levelSelectPanel.SetActive(false);
        anim.SetTrigger("ToMainMenu");
    }
    
    public void QuitGame()
    {
        Application.Quit();
    }

    public void LoadGame(int level)
    {
        SceneManager.LoadScene("TokyoGame" + level);
    }
}
