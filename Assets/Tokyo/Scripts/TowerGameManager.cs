﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class TowerGameManager : MonoBehaviour
{

	private static TowerGameManager _gameManager;

	public static TowerGameManager Instance
	{
		get { return _gameManager; }
		set { _gameManager = value; }
	}

	public bool spawningHasFinished;
	
	private int enemiesOnScene;
	public int EnemiesOnScene
	{
		get { return enemiesOnScene; }
		set
		{
			enemiesOnScene = value;
			Debug.Log("enemies on scene: " + enemiesOnScene);
			if (spawningHasFinished && enemiesOnScene == 0)
			{
				Debug.Log("Winner");
			}
		}
	}

	private void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
		}
		else
		{
			Destroy(gameObject);
		}
	}
}
